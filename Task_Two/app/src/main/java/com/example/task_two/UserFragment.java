package com.example.task_two;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

public class UserFragment extends Fragment {

    private  ArrayList <User> userList =  new ArrayList<User>();
    TextInputEditText firstNameTV;
    TextInputEditText lastNameTV;

    private RequestQueue requestQueue;

    public UserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }



    private void goToRecyclerView(){
        RecyclerViewFragment recyclerViewFragment = new RecyclerViewFragment();
       if(getFragmentManager()!=null)
        getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, recyclerViewFragment).
                addToBackStack("RECYCLER VIEW FRAGMENT").commit();
    }

    private void saveUser(View view){

        User user;
        String firstName = "";
        Boolean isFirstName = false;
        String lastName = "";
        Boolean isLastName = false;

        if(!firstNameTV.getText().toString().equals("")) {
            firstName = firstNameTV.getText().toString();
            isFirstName = true;
        }

        if(!lastNameTV.getText().toString().equals("")){
            lastName = lastNameTV.getText().toString();
            isLastName = true;
        }

        if(isFirstName && isLastName) {
            user = new User(firstName, lastName);
            new AsyncTaskInsert().execute(user);
        }
    }

    private void deleteUser(){

        User user;
        String firstName = "";
        Boolean isFirstName = false;
        String lastName = "";
        Boolean isLastName = false;

        if(!firstNameTV.getText().toString().equals("")) {
            firstName = firstNameTV.getText().toString();
            isFirstName = true;
        }

        if(!lastNameTV.getText().toString().equals("")){
            lastName = lastNameTV.getText().toString();
            isLastName = true;
        }

        if(isFirstName && isLastName) {
            user = new User(firstName, lastName);
            new AsyncTaskDeleteUser().execute(user);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        requestQueue = Volley.newRequestQueue(getContext());


      //  new  AsyncTaskInsert().execute(new User("BUCUR", "Andrei"));

      // DataBase.getDataBase(getContext()).userDao().insertAll(new User("dumitru", "fred"));

      //  userList = (ArrayList<User>) DataBase.getDataBase(getContext()).userDao().getAll();

       // new AsyncTaskGetAll().execute();

         firstNameTV = view.findViewById(R.id.first_name_TV);
         lastNameTV = view.findViewById(R.id.last_name_TV);

        Button buttonSyncServer = (Button) view.findViewById(R.id.btn_sync_with_server);
        Button buttonSaveUser = (Button) view.findViewById(R.id.btn_save);
        Button buttonDeleteUser = (Button) view.findViewById(R.id.btn_delete);

        buttonSaveUser.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                saveUser(view);
            }
        });

        buttonSyncServer.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view){
                jsonParse();
                goToRecyclerView();
            }
        });

        buttonDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteUser();
            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void jsonParse(){

        RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
        String url ="https://jsonplaceholder.typicode.com/users";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int index=0; index < jsonArray.length(); ++index)
                            {
                                if(jsonArray.get(index) instanceof JSONObject)
                                {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(index);

                                    String name = jsonObject.getString("name");
                                    StringTokenizer tokenizer = new StringTokenizer(name);
                                    User user = new User(tokenizer.nextToken(), tokenizer.nextToken());
                                    new AsyncTaskInsert().execute(user);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        queue.add(stringRequest);
    }


    private class AsyncTaskDeleteUser extends AsyncTask<User, Void, Void> {

        User user = null;
        @Override
        protected Void doInBackground(User... users) {

              user = DataBase.getDataBase(getContext()).userDao().findByName(users[0].firstName, users[0].lastName);
              if(user != null)
                 DataBase.getDataBase(getContext()).userDao().delete(user);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(user != null)
                Toast.makeText(getContext(), "User deleted"  , Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getContext(), "Can't find this user"  , Toast.LENGTH_SHORT).show();
        }
    }


    private class AsyncTaskInsert extends AsyncTask<User, Void, Void> {

        @Override
        protected Void doInBackground(User... users) {

            for(User user : users)
                DataBase.getDataBase(getContext()).userDao().insertAll(user);
            return null;
        }
    }

    private class AsyncTaskGetAll extends AsyncTask<Void, Void, Void>{


        @Override
        protected Void doInBackground(Void... voids) {
             userList = (ArrayList<User>) DataBase.getDataBase(getContext()).userDao().getAll();
             return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public void setUserList(ArrayList<User> users) {
        this.userList.clear();
        for(User user:users)
            this.userList.add(user);
    }
}
