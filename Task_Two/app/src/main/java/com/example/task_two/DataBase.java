package com.example.task_two;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class DataBase extends RoomDatabase {
    public abstract DAO userDao();

    private static volatile DataBase DatabaseInstance;

    static DataBase getDataBase(final Context context) {
        if (DatabaseInstance == null) {
            synchronized (DataBase.class) {
                if (DatabaseInstance == null) {
                    DatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                            DataBase.class, "User_DATABASE")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return DatabaseInstance;
    }
}
