package com.example.task_two;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {

    @PrimaryKey(autoGenerate = true)
    public int uid = 0;

    @ColumnInfo( name = "first_name")
    public String firstName;

    @ColumnInfo( name = "last_name")
    public String lastName;

    User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
