package com.example.task_two;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import kotlin.reflect.KVariance;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";
    private ArrayList<User> users = new ArrayList<User>();


    public RecyclerViewAdapter(ArrayList<User> users) {

        this.users = users;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView firstNameTV;
        private TextView lastNameTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            firstNameTV = itemView.findViewById(R.id.tv_first_name);
            lastNameTV = itemView.findViewById(R.id.tv_last_name);

        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent,
                false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.firstNameTV.setText(users.get(position).firstName);
        holder.lastNameTV.setText(users.get(position).lastName);

    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
