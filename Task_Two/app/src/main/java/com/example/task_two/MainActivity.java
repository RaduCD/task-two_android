package com.example.task_two;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        goToUserFragment();
    }

    private void goToUserFragment(){
        UserFragment userFragment = new UserFragment();
        getSupportFragmentManager().beginTransaction().
                add(R.id.fragmentContainer, userFragment).commit();
    }
}
